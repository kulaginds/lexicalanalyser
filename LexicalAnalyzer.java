package edu.sevgu.kulaginds.spo.compiler.lexical;

import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.Stack;

/**
 * Created by Dmitry on 06.05.2016.
 */
public class LexicalAnalyzer {

    private int states_count = 0; // количество состояний автомата (столбцы)
    private int chars_count = 0; // количество символов автомата (строки)
    private char[] chars; // массив рабочих символов
    private int[][] table; // таблица переходов и выходов
    private Lexeme list = null; // список выделенных лексем

    private boolean debug;

    public LexicalAnalyzer(char[] chars, int[][] table) {
        this(chars, table, false);
    }

    public LexicalAnalyzer(char[] chars, int[][] table, boolean debug) {
        if (chars == null
                || chars.length == 0)
            throw new RuntimeException("input missing: chars");
        if (table == null
                || table.length == 0)
            throw new RuntimeException("input missing: table");
        this.chars = chars;
        this.table = table;
        this.debug = debug;
        states_count = table[0].length;
        chars_count = chars.length;
        for (int i=0; i<chars_count; i++)
            if (table[i].length != states_count)
                throw new RuntimeException("input missing: table in line " + (i + 1));
    }

    public LexicalAnalyzer(String config_file, String code_file) throws IOException, LexicalException {
        this(config_file, code_file, false);
    }

    public LexicalAnalyzer(String config_file, String code_file, boolean debug) throws IOException, LexicalException {
        this.debug = debug;
        // загрузка пределов, списка символов и ТПиВ
        init(config_file);
        // разбор файла с кодом
        String code = loadCode(code_file);
        parse(code);
    }

    /**
     * Возвращает список лексем.
     * @return Lexeme
     */
    public Lexeme getLexemesList() {
        return list;
    }

    /**
     * Инициализация класса:
     *  - загрузка количества состояний
     *  - загрузка количества рабочих символов;
     *  - инициализация рабочих массивов;
     *  - загрузка рабочих символов;
     *  - загрузка таблицы переходов и выходов.
     * @param config_file
     * @throws IOException
     */
    protected void init(String config_file) throws IOException {
        RandomAccessFile f = new RandomAccessFile(config_file, "r");
        int i=0, j=0, tmp = 0;

        // загрузка количества состояний
        states_count = (int)f.readByte();
        // загрузка количества рабочих символов
        chars_count = (int)f.readByte();

        // инициализация массива рабочих символов
        chars = new char[chars_count];
        // инициализация таблицы переходов и выходов
        table = new int[chars_count][states_count];

        // заполняем массив рабочих символов
        for (i=0; i<chars_count; i++)
            chars[i] = (char)f.readByte();

        // заполняем таблицу переходов и выходов
        for (i=0; i<chars_count; i++)
            for (j=0; j<states_count; j++) {
                // читаем очередной байт
                tmp = 0xFF & f.readByte();

                if (tmp == 0xFF) { // если ячейка с признаком ошибки
                    f.readByte(); f.readByte();
                    tmp = -1;
                } else { // если нормальная ячейка
                    tmp = tmp << 2 * 8; // следующее состояние
                    tmp += f.readByte() << 1 * 8; // выделяем лексему
                    tmp += f.readByte(); // выделяем сдвиг
                }
                table[i][j] = tmp;
            }
        f.close();
    }

    /**
     * Загружает исходный файл с кодом.
     * @param code_file
     * @return code
     * @throws IOException
     */
    protected String loadCode(String code_file) throws IOException {
        String line, code = null;
        RandomAccessFile f = new RandomAccessFile(code_file, "r");
        // читаем построчно файл с кодом, удаляя невидимые символы типа
        // пробелов, табуляции и переносов строки
        while (null != (line = f.readLine()))
            if (null == code)
                code = line;
            else
                code += '\n' + line;
        f.close();
        code = code.replaceAll("\\s+", " ");
        return code;
    }

    /**
     * Производит разбор кода на лексемы.
     * Результат сохраняет в список.
     * @param code
     * @throws LexicalException
     */
    public void parse(String code) throws LexicalException {
        int state = 0, cell, char_index, lexeme_code;
        boolean shift = false;
        char input_char;
        Stack stack = new Stack();
        Lexeme lexeme, last_lexeme = null;

        // читаем посимвольно строку с кодом
        for (int i=0; i<code.length(); i++) {
            input_char = code.charAt(i);
            if (input_char != ' ')
                stack.push(input_char);
            char_index = getCharIndex(input_char);

            if (-1 == char_index) // если символ не из рабочих
                throw new LexicalException(LexicalException.UNKNOWN_CHAR);

            cell = table[char_index][state];

            if (-1 == cell) // если ячейка с признаком ошибки
                throw new LexicalException(LexicalException.ERROR_STATE);

            state = 0xFF & cell;
            lexeme_code = 0xFF & (cell >> 1 * 8);
            shift = (0xFF & (cell >> 2 * 8)) == 0xCC;

            if (lexeme_code > 0) { // если выделена лексема
                if (shift) // и есть флаг сдвига
                    stack.pop(); // выкидываем последний символ

                lexeme = new Lexeme(lexeme_code, getStackChars(stack));

                if (debug)
                    System.out.printf((list == null)?"L%d(%s)":", L%d(%s)", lexeme_code, lexeme.getData());

                if (list == null) // если первая лексема
                    list = last_lexeme = lexeme;
                else {
                    last_lexeme.next = lexeme;
                    last_lexeme = lexeme;
                }
                if (shift)
                    i--; // надо обработать символ, который выкинули из стека
            }
        }

        if (!stack.empty())
            throw new LexicalException(LexicalException.LAST_LEXEME);

        last_lexeme.next = new Lexeme(0);

        if (debug)
            System.out.println();
    }

    /**
     * Производит поиск в массиве рабочих символов
     * и возвращает индекс, если нашел. Иначе -1.
     * @param c
     * @return int
     */
    protected int getCharIndex(char c) {
        for (int i=0; i<chars_count; i++)
            if (c == chars[i])
                return i;
        return -1;
    }

    /**
     * Преобразовывает стек с символами в строку.
     * По ходу пьессы переворачивает строку.
     * @param stack
     * @return
     */
    protected String getStackChars(Stack stack) {
        int size = stack.size();
        char[] output = new char[size];
        for (int i=size - 1; i>=0; i--)
            output[i] = (char)stack.pop();
        return String.valueOf(output);
    }

}

package edu.sevgu.kulaginds.spo.compiler.lexical;

/**
 * Created by Dmitry on 06.05.2016.
 */
public class LexicalException extends Exception {

    static final String UNKNOWN_CHAR = "unknown char";
    static final String ERROR_STATE = "error state";
    static final String LAST_LEXEME = "do not select the last lexeme";

    public LexicalException(String msg) {
        super(msg);
    }

}

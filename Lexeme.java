package edu.sevgu.kulaginds.spo.compiler.lexical;

/**
 * Created by Dmitry on 06.05.2016.
 */
public class Lexeme {

    private int id;

    private String data;

    public Lexeme next;

    public Lexeme(int id) {
        this(id, null);
    }

    public Lexeme(int id, String data) {
        this.id = id;
        this.data = data;
        next = null;
    }

    public int getId() {
        return id;
    }

    public String getData() {
        return data;
    }

}
